# 🤖  Datawarehouse Triager 🤖

💩 Detect and report pipeline failures.

This project aims to automatically triage common pipeline failures. Using simple checks and regexes from [cki-lib](https://gitlab.com/cki-project/cki-lib), datawarehouse-triager is able to report failures on the Datawarehouse.

## How to run

Make sure your env has the following values:
- DATAWAREHOUSE_URL 
- GITLAB_TOKEN

To run the script, just execute the run.py file with a python3 interpreter.

```bash
export DATAWAREHOUSE_URL='http://0.0.0.0:8000'
export GITLAB_TOKEN=aBcDeFgHiJkL-1234567 
python3 run.py 
```

## How to test
There's no Docker image yet. Just install the dependencies on a venv or container and voilá.
```bash
apt-get update
apt-get install -y python3-pip
pip3 install -r requirements.txt
python3 -m unittest .
```

Before submiting changes, please lint the changes. Otherwise, it's gonna be 🔴 🤷.

```bash
flake8 .
pylint triager tests
```

## How to Contribute

To add new checks, take a look at the [TestFailureChecker](https://gitlab.com/cki-project/datawarehouse-triager/blob/main/triager/checkers.py).

And, **PLEASE**, include tests.
