"""Triager checkers."""
from cki_lib.logger import get_logger

from settings import FAIL_KICKSTART
from triager import regexes

from . import session

LOGGER = get_logger('cki.triager.checkers')
REGEX_CHECKER = regexes.RegexChecker()


class FailureChecker:
    """FailureChecker Class."""

    check_functions = (
        'check_logs_with_regex',
    )

    def __init__(self, obj):
        """Init."""
        self.obj = obj

    @property
    def logfiles(self):
        """
        Return a list of logfiles.

        Use output_files as a list of files to check.
        This method can be overriden on inherited classes to include different files.
        """
        try:
            return self.obj.output_files
        except KeyError:
            return []

    @classmethod
    def check(cls, obj):
        """Perform checks and return result."""
        checker = cls(obj)
        return checker.check_all()

    def check_all(self):
        """Run the checks."""
        failures = []
        for function in self.check_functions:
            LOGGER.info(' running: %s', function)
            result = getattr(self, function)()
            LOGGER.info('  result: %s', result)
            if result:
                failures.extend(result)

        LOGGER.info(' overall result: %s', failures)
        return failures

    def check_logs_with_regex(self):
        """Use regexes to find failures."""
        REGEX_CHECKER.download_lookups()
        failures = []
        for file in self.logfiles:

            if not (file['name'].endswith('.log') or
                    'journalctl' in file['name'] or
                    'TraceLog' in file['name']):
                # Not a log file.
                continue

            log_content = session.get(file['url']).content.decode(errors='ignore')
            failure = REGEX_CHECKER.search(log_content, file, self.obj)

            if failure:
                failures.extend(failure)

        return failures


class BuildFailureChecker(FailureChecker):
    """BuildFailureChecker Class."""

    @property
    def logfiles(self):
        """Return the build log files."""
        try:
            return [
                {'name': 'build.log', 'url': self.obj.log_url}
            ]
        except KeyError:
            return []


class TestFailureChecker(FailureChecker):
    """TestFailureChecker Class."""

    check_functions = (
        FailureChecker.check_functions +
        (
            'check_kickstart_error',
        )
    )

    def check_kickstart_error(self):
        """
        Check job failed to Kickstart.

        If 'Boot test' has no duration, failed to provision.
        """
        if self.obj.description == 'Boot test' and not self.obj.duration:
            return [FAIL_KICKSTART]

        return []
