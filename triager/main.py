#!/usr/bin/python3
"""Datawarehouse failure triager."""
import argparse

from cki_lib import messagequeue
from cki_lib import metrics
from cki_lib import misc
from cki_lib.logger import get_logger
import prometheus_client as prometheus
import sentry_sdk

import settings
from triager import triager

LOGGER = get_logger('cki.triager')
IS_PRODUCTION = misc.is_production()
MESSAGES_TRIAGE = ('new', 'needs_triage')

METRIC_MESSAGE_RECEIVED = prometheus.Counter(
    'message_received', 'Number of queue messages received')
METRIC_MESSAGE_PROCESS = prometheus.Counter(
    'message_processed', 'Number of queue messages processed')
METRIC_MESSAGE_IGNORED = prometheus.Counter(
    'message_ignored', 'Number of queue messages ignored')
METRIC_LOAD = metrics.LoadIndex(
    prometheus.Summary(
        'processing_load', 'Normalized indicator of the time spent processing a message'
    )
)


@METRIC_LOAD.context()
def callback(body, ack_fn, dry_run=False):
    """Handle a single message."""
    METRIC_MESSAGE_RECEIVED.inc()
    obj = body['object']
    obj_type = body['object_type']
    status = body['status']
    LOGGER.info('Got message for (%s) %s id=%s', status, obj_type, obj['id'])
    if status in MESSAGES_TRIAGE:
        triager.Triager(dry_run).check(obj_type, obj)
        METRIC_MESSAGE_PROCESS.inc()
    else:
        METRIC_MESSAGE_IGNORED.inc()

    if not dry_run:
        ack_fn()


def triage_queue(dry_run=False):
    """Triage elements received using the message queue."""
    LOGGER.info("Running checks on queue items.")
    queue = messagequeue.MessageQueue()
    queue.consume_messages(settings.EXCHANGE_NAME, ['#'],
                           lambda _, body, ack_fn: callback(body, ack_fn, dry_run),
                           queue_name=settings.MESSAGE_QUEUE_NAME,
                           manual_ack=True)


def triage_single(obj_type, obj_id, dry_run=False):
    """Triage a single object."""
    triager.Triager(dry_run).check(obj_type, obj_id)


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--dry-run', action='store_true', default=(not IS_PRODUCTION),
                        help='Just look for issues. Do not create failure reports.')
    subparsers = parser.add_subparsers(dest='strategy')
    # Queue parsing subperser.
    subparsers.add_parser('queue')
    # Single object parsing subperser.
    parser_single = subparsers.add_parser('single')
    parser_single.add_argument(
        'type', choices=['revision', 'build', 'test'], help='Kind of stuff to check'
    )
    parser_single.add_argument(
        'id', help='Id or iid of the object to check'
    )

    return parser.parse_args()


if __name__ == '__main__':
    arguments = parse_args()

    misc.sentry_init(sentry_sdk)
    metrics.prometheus_init(prometheus)

    if not arguments.strategy or arguments.strategy == 'queue':
        triage_queue(arguments.dry_run)
    if arguments.strategy == 'single':
        triage_single(arguments.type, arguments.id, arguments.dry_run)
